<?php

//пришлось написать роутер который адрессную строку разбирает не по слэшах "/", а по аперсандах "&" 

  $url = $_SERVER['REQUEST_URI'];
  
  $url = rtrim($url, '/');  //обрезаем последний слэш, если он есть
  $url = explode('/', $url); //разрезаем по слэшам
  $url = explode('&', $url[2]); //последний кусок("index.php?option=com_mytests&...&...") режем по "&"
  
  //если ничего нет после "index.php?option=com_mytests", то это страница по умолчанию
  if(!isset($url[1])){
      require_once (ROOT.'/controllers/MytestsController.php');
      $controller = new MytestsController();  
      $controller->actionQuestions();    //по умолчанию загружается страница со списком вопросов
  }else{                                   
      $nameController = $url[1].'Controller';  //иначе первое слово после "&"  будет контролером
      require_once (ROOT.'/controllers/'.$nameController.'.php');
      $controller = new $nameController;
  }
  if(isset($url[2])&&isset($url[3])) { //если есть второй и третий "&", то за ними это будет метод контролера и аргумет
      $action = 'action'.$url[2]; 
      $data = $url[3];
      $data = explode('=', $data);
      $controller->$action($data[1]);  //запускаем метод с параметрами 
  }
  else {
       if(isset($url[2])) {           //иначе если аргуметов нет то запускаем метод без них
        $action = 'action'.$url[2];

        $controller->$action();
       }
  }