<?php
require_once ROOT.'/models/Questions.php';
require_once ROOT.'/models/Answers.php';
require_once ROOT.'/models/Search.php';

class MytestsController {
    //страница списка вопросов
    public function actionQuestions(){
        $questionList = Questions::getQuestionList();
        require_once ROOT.'/views/questions.php';
    }
    //страница редактирования и создания вопроса
    public function actionQuestionEdit($id=null){
       $question = Questions::getQuestionById($id);
       require_once ROOT.'/views/question_edit.php';
    }
    //удаление вопроса
    public function actionQuestionDelete($id=null){
       Questions::deleteQuestion($id);
       header('Location: ?option=com_mytests');
    }
    //страница со списком ответов текущего вопроса
    public function actionAnswers($id){
        $question = Questions::getQuestionById($id);
        $answersList = Answers::getAnswerList($id);
        require_once ROOT.'/views/answers.php';
    }
    //страница редактирования ответа
    public function actionAnswerEdit($id=null){        
            $answer = Answers::getAnswerById($id);
            require_once ROOT.'/views/answer_edit.php';        
    }
    //страница создания ответа
    public function actionAnswerCreate($id_quest=null){        
            $answer = Answers::getAnswerById($id_quest);
            require_once ROOT.'/views/answer_create.php';        
    }
    //удаление ответа
     public function actionAnswerDelete($id=null){
         $answer = Answers::getAnswerById($id);         
         Answers::deleteAnswer($id);
       header('Location: ?option=com_mytests&mytests&answers&id='.$answer->id_question);
    }
    
    //поиск по вопросам
    public function actionSearch(){
        require_once ROOT.'/views/search.php';
    }
    //поиск по ответам текущего вопроса
    public function actionSearchAnsw($id){
        require_once ROOT.'/views/search_answ.php';
    }




    //ЗАПРОСЫ ЧЕРЕЗ JQERY
    
    //задать правильный ответ
    public function actionAnswerRight(){
       if(isset($_GET['id'])&& isset($_GET['id_answ'])){
           $id = $_GET['id'];
           $id_answ = $_GET['id_answ'];
           Questions::setRightAnswer($id, $id_answ); 
       }
      return TRUE;
    }
    
    // ответ oпубликовать или снять с публикации 
    public function actionAnswerPublic(){
       if(isset($_GET['id'])){
           $id = $_GET['id'];
           if(isset($_GET['val']) && $_GET['val']==1){
           $val = 0;            
           }else{
            $val =1;   
           } 
            Answers::setPublicAnswer($val,$id);
       }
      return $val;
    }
    // вопрос oпубликовать или снять с публикации 
    public function actionQuestionPublic(){
       if(isset($_GET['id'])){
           $id = $_GET['id'];
           if(isset($_GET['val']) && $_GET['val']==1){
           $val = 0;            
           }else{
            $val =1;   
           } 
            Questions::setPublicQuestion($val,$id);
       }
      return $val;
    }
    
}
