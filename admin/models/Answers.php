<?php
/**
 * Description of Answers
 *
 * @author Миколка
 */
class Answers {
    //получить список ответов
     public static function getAnswerList($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__answers');
        $query->where('id_question='.$id);
        $results = $db->setQuery($query)->loadObjectList();
        return $results;
    }
    //получить ответ по его id
     public static function getAnswerById($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__answers');
        $query->where('id="'.$id.'"');
        $results = $db->setQuery($query)->loadObject();
        return $results;
    }
    //создать ответ для текущего вопроса
    public static function createAnswer($id_quest,$data){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->insert('#__answers');
        $query->columns('content, id_question');
        $query->values('"'.$data.'","'.$id_quest.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
   //изменить ответ
    public static function updateAnswer($id,$data,$publish){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->update('#__answers');
        $query->set('content="'.$data.'",publish="'.$publish.'"');
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    //удалить ответ
     public static function deleteAnswer($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete('#__answers');
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    //удалить несколько ответов
    public static function deleteAnswerList($args){
        foreach($args as $arg){
            $str.= ' id="'.$arg. '" OR';
        }
        $str = rtrim($str,'OR');
        $db = JFactory::getDbo();
        $query= 'DELETE FROM #__answers WHERE '.$str;
        $db->setQuery($query);
        $db->execute();
        header('Location: ?option=com_mytests');
        return true;
    }
    //опубликовать/снять несколько ответов
    public static function setPublishList($val=0,$args){
        $db = JFactory::getDbo();
       foreach ($args as $arg){
           $str .= '('.$arg.','.$val.'),';
        }
        $str = rtrim($str,',');
        $query= 'INSERT INTO #__answers (id, publish) VALUES '.$str.
                '  ON DUPLICATE KEY UPDATE publish = VALUES (publish)';
        $db->setQuery($query);
        $db->execute();
        echo $query;
        return true;
    }
    //опубликовать/снять ответ
    public static function setPublicAnswer($val,$id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->update('#__answers');
        $query->set('publish='.$val);
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    
    
}
