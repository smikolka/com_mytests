<?php

/**
 * Description of Questions
 *
 * @author Миколка
 */
class Questions {
    //получить список вопросов вместе с количеством ответов к каждому вопросу
    public static function getQuestionList(){
        $db = JFactory::getDbo();
        $query = 'SELECT #__questions.*, count(#__answers.id_question) FROM #__questions 
                    left outer join #__answers ON #__questions.id = #__answers.id_question
                    GROUP BY #__questions.id';
        $results = $db->setQuery($query)->loadObjectList();
        return $results;
    }
    //получить вопрос по его id
    public static function getQuestionById($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__questions');
        $query->where('id="'.$id.'"');
        $results = $db->setQuery($query)->loadObject();
        return $results;
    }
    //создать вопрос
    public static function createQuestion($data){
       // echo $value;die;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->insert('#__questions');
        $query->columns('content');
        $query->values('"'.$data.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    //изменить вопрос
    public static function updateQuestion($id,$data,$publish){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->update('#__questions');
        $query->set('content="'.$data.'",publish="'.$publish.'"');
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    //удалить вопрос
    public static function deleteQuestion($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete('#__questions');
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    //удалить несколько вопросов
    public static function deleteQuestionList($args){
        foreach($args as $arg){
            $str.= ' id="'.$arg. '" OR';
        }
        $str = rtrim($str,'OR');
        $db = JFactory::getDbo();
        $query= 'DELETE FROM #__questions WHERE '.$str;
        $db->setQuery($query);
        $db->execute();
        header('Location: ?option=com_mytests');
        return true;
    }
    //опубликовать/снять несколько вопросов
    public static function setPublishList($val=0,$args){
        $db = JFactory::getDbo();
       foreach ($args as $arg){
           $str .= '('.$arg.','.$val.'),';
        }
        $str = rtrim($str,',');
        $query= 'INSERT INTO #__questions (id, publish) VALUES '.$str.
                '  ON DUPLICATE KEY UPDATE publish = VALUES (publish)';
        $db->setQuery($query);
        $db->execute();
         echo $query;
        header('Location: ?option=com_mytests');
        return true;
    }
    //задать правильный ответ для даного вопроса
    public static function setRightAnswer($id,$id_answ){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->update('#__questions');
        $query->set('right_answer="'.$id_answ.'"');
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
    //опубликовать/снять вопрос
    public static function setPublicQuestion($val,$id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->update('#__questions');
        $query->set('publish='.$val);
        $query->where('id="'.$id.'"');
        $db->setQuery($query);
        $db->execute();
        return true;
    }
   
}
