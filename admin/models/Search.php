<?php
class Search {
   public static function getSearchResult($id=null){
      if (!empty($_POST['query'])){
          $q=$_POST['query'];
          $q = trim($q);
          $q = htmlspecialchars($q);
          $words = explode(' ', $q); //если запрос из нескольких слов, разделяем на слова
        $db = JFactory::getDbo();
        if(isset($_POST['for_answers'])){  //если нажата кнопка на страницах отображающих ответы
            
         //искать среди ответов текущего вопроса
         $query = 'SELECT * FROM #__answers '
                 .'WHERE id_question="'.$id.'" AND '; //начало запроса
            foreach ($words as $word){
                $query .= ' content LIKE "%'.$word.'%" OR';
            }
                $query = rtrim($query, 'OR');  
        }else{   
            
        //искать среди вопросов (получаем также количество ответов для каждого вопроса)
        $query = 'SELECT #__questions.*, count(#__answers.id_question) FROM #__questions '
                    .'left outer join #__answers ON #__questions.id = #__answers.id_question'
                    .' WHERE'; //начало запроса
            foreach ($words as $word){
                $query .= ' #__questions.content LIKE "%'.$word.'%" OR';
            }
                $query = rtrim($query, 'OR');
                $query .= ' GROUP BY #__questions.id' ; //конец запроса
        }
         
         $result = $db->setQuery($query)->loadObjectList();
       
         return $result;
         }
         
        return false;      
   }
    
   
   
}