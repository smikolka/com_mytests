<div class="head"><div>
    <img src="/administrator/components/com_mytests/template/images/point.png"/>
    <span>Answer Edit</span></div>
</div>
<div class="panel edit">
    <ul>
        <li>
              <button form="form" type="submit" name="Save and close" value="Save and close" >
               <img src="/administrator/components/com_mytests/template/images/v.png"/>
               Save&Close
              </button>
        </li>
        <li>
            <button id="save" form="form" type="submit" name="Save" value="Save" >
            <img src="/administrator/components/com_mytests/template/images/editg.png"/>
            Save
            </button>
        </li>
        <li class="cansel"><a href="?option=com_mytests&mytests&answers&id=<?=$answer->id_question?>">
                <img src="/administrator/components/com_mytests/template/images/x.png"/>
                Cansel</a></li>
    </ul>
</div>
<div class="content">

        
    <form method="post"  id="form">
        <ul class="edit form">
            <li><div>Publish</div> <input type="checkbox" name="publish" <?php echo $answer->publish==1 ? "checked" : "" ?>/></p></li>                        
            <li><div>Answer</div> <textarea name='content'><?php echo $answer->content ?></textarea></li>
        </ul>
    </form>
       
</div>

<?php 

if ($_REQUEST['id']&&$_REQUEST['content']){
    $id = $_REQUEST['id'];
    $content = $_REQUEST['content'];
    if($_REQUEST['publish']){
        $publish = 1;
    }else{
        $publish = 0;
    }
    Answers::updateAnswer($id,$content,$publish);
    if($_REQUEST['Save']){ //если нажата кнопка "сохранить", то возвращаем на ту же страницу 
       header('Location: ?option=com_mytests&mytests&answeredit&id='.$answer->id);
    }else{
        //иначе уходим к списку ответов текущего вопроса
      header('Location: ?option=com_mytests&mytests&answers&id='.$answer->id_question);
    }
}

?>