<!--это страница результата поиска в списке вопросов-->
<div class="head"><div>
    <img src="/administrator/components/com_mytests/template/images/point.png"/>
    <span>Result of Search</span></div>
</div>
<div class="panel">
    <ul>
        
        <li><button form="form" type="submit" name="publish" value="Publish" >
                <img src="/administrator/components/com_mytests/template/images/v.png"/>
                Publish</button></li>
        <li><button form="form" type="submit" name="unpublish" value="Unpublish">
                <img src="/administrator/components/com_mytests/template/images/x.png"/>
                Unpublish</button></li>
        <li><button form="form" type="submit" name="delete" value="Delete">
                <img src="/administrator/components/com_mytests/template/images/bx.png"/>
                Delete</button></li>
        <li class="cansel"><a href="?option=com_mytests">
                <img src="/administrator/components/com_mytests/template/images/x.png"/>
                Cansel</a></li>
    </ul>
</div>
<div class="search">
    <form name="search" action="?option=com_mytests&mytests&search" method="post">
        <div> <input size="25" type="text" placeholder="<?php echo "Search..."; ?>" class="search" id="inpute" name="query"></div>
        <button class="bsubmit" type="submit">
            <img src="/administrator/components/com_mytests/template/images/magnifier.png"/>
        </button>
        <button class="breset" type="reset">
            <img src="/administrator/components/com_mytests/template/images/bigbx.png"/>
        </button>
    </form>   
</div> 
                
<?php if(!Search::getSearchResult()==false ) : ?>
                <?php $resultSearch=Search::getSearchResult();?>
                <h3><?php echo "Найдено результатов"; ?>  <?php echo count($resultSearch); ?></h3>

<div class="content">
    <form id="form" method="post">
        <table class="zebra">
            <tr class="htable">
                <td>#</td>
                <td><input id="allcheckbox" type="checkbox"></td>
                <td><span>Question List</span></td>
                <td><span>Answers</span></td>
                <td>Published</td>
                <td>Edit</td>
                <td>Delete</td>
                <td>ID</td>
            </tr>
            <?php $i=1; ?>
            <?php foreach ($resultSearch as $question): ?>
            <tr>
               <td><?php echo $i ?></td>
               <td class="check"><input  type="checkbox" name="<?=$i?>" value ="<?=$question->id?>" ></td>
                <td>
                    <a href="?option=com_mytests&mytests&questionedit&id=<?= $question->id?>">
                        <p><?php echo $question->content?></p>
                    </a>
                </td>
                <td>
                    <a href="?option=com_mytests&mytests&answers&id=<?= $question->id ?>">
                        <span>Answers</span> (<?php echo end($question); ?>)
                </td>
                <td> 
                    <div class="publ" id="qpubl<?=$question->id?>" data-id="<?=$question->id?>" data-val="<?=$question->publish?>">
                    <img src="/administrator/components/com_mytests/template/images/<?php echo $question->publish == 1 ? 'u44.PNG' :'u52.PNG'?>" />
                    </div>
                </td>
                <td><a href="?option=com_mytests&mytests&questionedit&id=<?=$question->id?>">
                        <img src="/administrator/components/com_mytests/template/images/edit.png"/>
                    </a>
                </td>
                <td>
                    <a href="?option=com_mytests&mytests&questiondelete&id=<?=$question->id?>">
                        <img src="/administrator/components/com_mytests/template/images/delete.png"/>
                    </a>
                </td>
                <td><?php echo $question->id ?></td> 
                 
            </tr>
            
            <?php $i++;?>
            <?php endforeach; ?>
    </table>
    </form>
    
</div>
<?php endif; ?>

<?php
if (Search::getSearchResult() == false) {
    echo "<h3>Ничего не найдено</h3>";
}
?> 

<?php
//если были заполнены некоторые checkbox и
//была нажата одна из  кнопок "опубликовать", "неопубликовывать", "удалить"
if($_POST['publish']){
    $args = $_POST;
    $args = array_slice($args, 1);
    if($args){
    Questions::setPublishList(1,$args);
    }
}elseif($_POST['unpublish']){
    $args = $_POST;
    $args = array_slice($args, 1);
    if($args){
    Questions::setPublishList(0,$args);
    }
}elseif($_POST['delete']){
    $args = $_POST;
    $args = array_slice($args, 1);
    if($args){
     Questions::deleteQuestionList($args);
    }
}
?>

<script>
//опубликовать или снять с публикации
$(document).ready(function(){
         $('.publ').on('click',function () {
             var id = $(this).data('id');
             var val = $(this).data('val');
            $.get('?option=com_mytests&mytests&questionpublic&val='+val+'&id='+id, {}, function () {
               if($("#qpubl"+id).data('val')==0){
                  $("#qpubl"+id).html('<img src="/administrator/components/com_mytests/template/images/u44.PNG"/>');
                  $("#qpubl"+id).data('val',1);
        }else{
                  $("#qpubl"+id).html('<img src="/administrator/components/com_mytests/template/images/u52.PNG"/>');    
                  $("#qpubl"+id).data('val',0);     
              };
            
            });
            return false;
        });
   });

</script>
<script>
//вибрать все вопросы или снять 
$(document).ready(function(){
         $('#allcheckbox').on('click',function () {
             if($(".check input").prop('checked')== false){
              $(".check input").prop('checked', true);
          }else{
              $(".check input").prop('checked', false);
          }
              
        });
   });

</script>