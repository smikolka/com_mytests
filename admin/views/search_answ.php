<!--это страница результатов поиска в списке ответов-->
<div class="head"><div>
    <img src="/administrator/components/com_mytests/template/images/point.png"/>
    <span>Result of Search</span></div>
</div>
<div class="panel">
    <ul>
        
        <li><button form="form" type="submit" name="publish" value="Publish" >
                <img src="/administrator/components/com_mytests/template/images/v.png"/>
                Publish</button></li>
        <li><button form="form" type="submit" name="unpublish" value="Unpublish">
                <img src="/administrator/components/com_mytests/template/images/x.png"/>
                Unpublish</button></li>
        <li><button form="form" type="submit" name="delete" value="Delete">
                <img src="/administrator/components/com_mytests/template/images/bx.png"/>
                Delete</button></li>
        <li class="cansel"><a href="?option=com_mytests&mytests&answers&id=<?=$id?>">
                <img src="/administrator/components/com_mytests/template/images/x.png"/>
                Cansel</a></li>
    </ul>
</div>
<div class="search">
    <form name="search" action="?option=com_mytests&mytests&searchansw&id=<?=$id?>" method="post">
        <div> <input size="25" type="text" placeholder="<?php echo "Search..."; ?>" class="search" id="inpute" name="query"></div>
        <button class="bsubmit" type="submit" name="for_answers">
            <img src="/administrator/components/com_mytests/template/images/magnifier.png"/>
        </button>
        <button class="breset" type="reset">
            <img src="/administrator/components/com_mytests/template/images/bigbx.png"/>
        </button>
    </form>   
</div> 
                
<?php if(!Search::getSearchResult($id)==false ) : ?>
                <?php $resultSearch=Search::getSearchResult($id);?>
                <h3><?php echo "Найдено результатов"; ?>  <?php echo count($resultSearch); ?></h3>

<div class="content">    
    <form id="form" method="post">
        <table class="zebra">
        <tr class="htable">
                <td>#</td>
                <td><input id="allcheckbox" type="checkbox"></td>
                <td><span>Answer List</span></td>
                <td>Select the right Answer</td>
                <td>Published</td>
                <td>Edit</td>
                <td>Delete</td>
                <td>ID</td>
            </tr>
            <?php $i=1; ?>
            <?php foreach ($resultSearch as $answer): ?>
            <tr>
               <td><?php echo $i ?></td>
               <td class="check"><input type="checkbox" name="<?=$i?>" value ="<?=$answer->id?>"></td>
                <td>
                    <a href="?option=com_mytests&mytests&answeredit&id=<?= $answer->id ?>">
                           <?php echo $answer->content ?> 
                    </a>
                </td>
                <td>
                     <div id="answ<?=$answer->id?>" class="ransw" data-id="<?=$id?>" data-answ="<?=$answer->id?>">
                         <img src="/administrator/components/com_mytests/template
                              /images/<?= $question->right_answer == $answer->id ? 'u145.PNG' :'u137.PNG'?>" />
                         
                    </div>
                </td>
                <td>
                    <div id="answpub<?=$answer->id?>" class="publ" data-id="<?=$answer->id?>" data-val="<?=$answer->publish?>">
                         <img src="/administrator/components/com_mytests/template
                              /images/<?= $answer->publish == 1 ? 'u44.PNG' :'u52.PNG'?>" />
                         
                    </div>
                </td>
                <td>
                    <a href="?option=com_mytests&mytests&answeredit&id=<?=$answer->id?>">
                        <img src="/administrator/components/com_mytests/template/images/edit.png"/>
                    </a>
                </td>
                <td>
                    <a href="?option=com_mytests&mytests&answerdelete&id=<?=$answer->id?>">
                         <img src="/administrator/components/com_mytests/template/images/delete.png"/>
                    </a>
                </td>
                <td><?php echo $answer->id ?></td> 
            </tr>
            
            <?php $i++;?>
            <?php endforeach; ?>
    </table>
    </form>
</div>
<?php endif; ?>

<?php
if (Search::getSearchResult($id) == false) {
    echo "<h3>Ничего не найдено</h3>";
}
?> 

<?php
//если были заполнены некоторые checkbox и
//была нажата одна из  кнопок "опубликовать", "неопубликовывать", "удалить"
if($_POST['publish']){
    $args = $_POST;
    $args = array_slice($args, 1); //отрезаем 'publish' из масива 
    if($args){
        Answers::setPublishList(1,$args);
        header('Location: ?option=com_mytests&mytests&answers&id='.$id);
    }
}elseif($_POST['unpublish']){
    $args = $_POST;
    $args = array_slice($args, 1);
    if($args){
        Answers::setPublishList(0,$args);
        header('Location: ?option=com_mytests&mytests&answers&id='.$id);
    }
}elseif($_POST['delete']){
    $args = $_POST;
    $args = array_slice($args, 1);
    if($args){
     Answers::deleteAnswerList($args);
     header('Location: ?option=com_mytests&mytests&answers&id='.$id);
    }
}
?>

<script>
    //задать праивильный ответ
$(document).ready(function(){
         $('.ransw').on('click',function () {
             var id = $(this).data('id');
             var id_answ = $(this).data('answ');
            $.get('?option=com_mytests&mytests&answerright&id='+id+'&id_answ='+id_answ, {}, function () {
                $(".ransw").html('<img src="/administrator/components/com_mytests/template/images/u137.PNG"/>');
                $("#answ"+id_answ).html('<img src="/administrator/components/com_mytests/template/images/u145.PNG"/>');
            });
            return false;
        });
   });
   
   
</script>
<script>
//опубликовать или снять с публикации
$(document).ready(function(){
         $('.publ').on('click',function () {
             var id = $(this).data('id');
             var val = $(this).data('val');
            $.get('?option=com_mytests&mytests&answerpublic&val='+val+'&id='+id, {}, function () {
               if($("#answpub"+id).data('val')==0){
                  $("#answpub"+id).html('<img src="/administrator/components/com_mytests/template/images/u44.PNG"/>');
                  $("#answpub"+id).data('val',1);
        }else{
                  $("#answpub"+id).html('<img src="/administrator/components/com_mytests/template/images/u52.PNG"/>');    
                  $("#answpub"+id).data('val',0);     
              };
            
            });
            return false;
        });
   });
  
</script>

<script>
//вибрать все ответы или снять 
$(document).ready(function(){
         $('#allcheckbox').on('click',function () {
             if($(".check input").prop('checked')== false){
              $(".check input").prop('checked', true);
          }else{
              $(".check input").prop('checked', false);
          }
              
        });
   });

</script>