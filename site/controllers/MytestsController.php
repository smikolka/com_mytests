<?php
require_once ROOT.'/models/Model.php';

class MytestsController{
    //страница вопросов
    public function actionQuestions(){
        $answerList = Model::getAnswerList(); //список ответов
        $questionList = Model::getQuestionList(); //список вопросов
        $countQuestions = count($questionList); //количество всех вопросов
        
        $nextPage = Model::getNextPage($countQuestions); //следующая страница
        require_once ROOT.'/views/questions.php';
    }
    //страница результата
    public function actionResult(){
        $questionList = Model::getQuestionList();  //список вопросов
        $countQuestion = count($questionList); //количество всех вопросов
        
        $points = Model::getResultTest($questionList); //количество правильных ответов
        $prosent = Model::getProsent($countQuestion, $points); //процентно правильных ответов от количества всех ответов
        require_once ROOT.'/views/result.php';
    }
    
}
