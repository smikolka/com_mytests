<?php
class Model{
    //получить список вопросов
    public static function getQuestionList(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__questions');
        $query->where('publish=1');
        $results = $db->setQuery($query)->loadObjectList();
        
        return $results;
    }
    //получить список ответов
    public static function getAnswerList(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__answers');
         $query->where('publish=1');
        $results = $db->setQuery($query)->loadObjectList();
        return $results;
    }
    //получить следующую страницу
    public static function getNextPage($count){
        
        if($_REQUEST['p']){        //если переменна 'p' есть (текущая страница)
            $page = $_REQUEST['p'];
            $page++;               //получае следующую траницу
            if($page>$count+1){    //если  закончились ответы
                $run = new MytestsController();
                $run->actionResult();  //переходим на страницу реузультатов
                return false;
            }
            return $page; // иначе отдаем следующую страницу 
        }else{
            return 2; //иначе это первая страница (первый вопрос), отдаем вторую
        }
    }
    //создаем и пополняем список ответов пользователя
    public static function addAnswerToListUser($numberQuestion){
        if ($_REQUEST['answer']){
            $_SESSION['answer'][$numberQuestion]= $_REQUEST['answer'];            
        }
    }
    //получить результат тестов
    public static function getResultTest($questionList){
        $answerUserList =  $_SESSION['answer'];
        $points = 0; //количество правильных ответов
        $i = 1; 
        foreach($questionList as $question){
            //проверяем правильность ответов пользователя 
                if($question->right_answer === $answerUserList[$i]){
                    $points +=1; //собираем  правильные ответы 
                }
            $i++;
        }
        unset($_SESSION['answer']);//удаляем сессию со списком ответов пользователя 
       return $points; //оттдаем количество правильных ответов
    }
    //вычисляем процентно качество совпадений 
    public static function getProsent($count, $points){
          $prosent = $points*100/$count;
          return intval($prosent);
    }
   
}
