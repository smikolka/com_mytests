
<!--по умолчанию $nextPage=2-->
<?php if($nextPage): ?> 
<div class="content">
<form method="post"  id="form">
<?php $i=1; ?>
<?php foreach ($questionList as $question):?>
<!--если $i равно текущей странице (нужно для того чтобы каждый вопрос отображался на отдельной странице)-->
<?php if  ($i==$nextPage-1):?>
      <!--отображаем вопрос текущий вопрос-->
    <h2><?php echo $question->content ?></h2> 

    <ul>
       <!-- переберая все ответы отображаем те, которые относятся к текущему вопросу-->
      <?php foreach ($answerList as $answer):?>
      <?php if ($answer->id_question == $question->id):?>
        <li><input type="radio" name="answer" value="<?=$answer->id; ?>" onclick="show_button();">
            <div><?= $answer->content;?></div>
        </li>
      <?php else:?>
      <?php endif;?>
      <?php endforeach;?>
        
    </ul>
 <?php endif; ?>
<?php $i++ ?>     
<?php endforeach; ?>
    
<div class="button"><button type="submit" id="button" style="display: none">Next Question ></button></div>
</form> 
</div>
<?php endif; ?>

<?php
if ($_REQUEST['answer']){  //если ответ был выбран и нажата кнопка следующий вопрос
    Model::addAnswerToListUser($nextPage-1); //записываем ответ пользователя в сессию
    header('Location: ?option=com_mytests&p='. $nextPage); //переход на следующую страницу, передача переменной 'p' по get запросу
}
?>


<script>
    //по нажатию на ответ отобразить кнопку Next Question 
function show_button()
{
var button = document.getElementById("button");
button.style.display = "inline";
}
</script>

